# React Pokedex

What if the pokedex was an app made in 2019 ? You can view this project [here](https://pokedex-78417.firebaseapp.com)

The UI is inspired of this [UI](https://dribbble.com/shots/6540871-Pokedex-App) and this [UI](https://dribbble.com/shots/2859891--025-Pikachu)

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.14.2

    $ npm --version
    6.11.2

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://gitlab.com/mdeken/react-pokedex.git
    $ cd react-pokedex
    $ npm install

## Start & watch

    $ npm start

## Simple build for production

    $ npm run build

---

## Languages & tools

### Data

The data are from the [PokeAPI](https://pokeapi.co/) endpoints.

The images of pokemons are from the official [Pokedex website](https://www.pokemon.com/fr/pokedex/). (The PokeAPI sprites were the Nintendo ones and were not pretty to integrates)

### JavaScript

- [React](http://facebook.github.io/react) is used for UI.
- [React Router](https://reacttraining.com/react-router/web) is used to handle routing in the app.
- [Redux](https://redux.js.org/) is used to seperate rendering logic from 'business' logic.
- [Redux Saga](https://redux-saga.js.org) is used to be able to write asynchronous code with Redux.
- [D3.js](https://d3js.org) is used to generate the bar charts of pokemon stats.
- [GSAP](https://greensock.com/gsap) is used to animate the spinner (Very overkill for this usecase but I wanted to see if it integrates with a React app)
 
For this project I wanted to challenge myself by using only React hooks and fetch API.


### CSS

- [SCSS](https://sass-lang.com) is used and all the CSS is home-made (No frameworks)