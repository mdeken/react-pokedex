import React, { useState, useEffect } from 'react';

import Modal from '../components/Modal/Modal';

const withModalError = (ParentComponent, error, modalChildren) => {
  return (props) => {
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
      if (error) {
        setShowModal(true);
      }
    }, []);
    
    const modal = (
      <Modal close={() => setShowModal(false)}>
        {modalChildren}
      </Modal>
    );

    return (
      <React.Fragment>
        { showModal ? modal : null }
        <ParentComponent {...props}/>
      </React.Fragment>
    );
  };
};

export default withModalError;