import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import './Pokedex.scss';

import * as actions from '../../store/actions';
import Card from '../../components/Card/Card';
import Button from '../../components/Button/Button';
import withModalError from '../../hoc/withModalError';
import Spinner from '../../components/Spinner/Spinner';
import { ReactComponent as Pokeball } from '../../assets/icons/pokeball.svg';

const Pokedex = () => {
  const pokemons = useSelector(state => state.pokemons);
  const error = useSelector(state => state.error);
  const page = useSelector(state => state.page);
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.loading);

  useEffect(() => {
    // Should fetch only the first page, on componentDidMount
    if (pokemons.length === 0) {
      dispatch(actions.fetchPokedex(0));
    }
  }, [dispatch, pokemons.length]);

  const pokemonCards = pokemons.map(pokemon => (
    <Card 
      key={pokemon.id}
      name={pokemon.name}
      id={pokemon.id}
      paddedId={pokemon.paddedId}
      type1={pokemon.type1}
      type2={pokemon.type2}
      sprite={pokemon.sprite}
    />
  ));

  const modalContent = (
    <div className="Modal-Content">
      <h2>Ooops...</h2>
      <p>An error occured...Retry later or contact Professor Oak.</p>
    </div>
  );

  const PokemonCardsWithModalError = withModalError(
    () => pokemonCards,
    error,
    modalContent
  );

  let nextButton = null;
  if (page && pokemons.length !== 807) {
    nextButton = <Button clicked={() => dispatch(actions.fetchPokedex(page))}>Load more</Button>;
  }

  return (
    <div className="Pokedex">
      <header className="Header">
        <h1>Pokedex</h1>
      </header>
      <Pokeball className="Pokeball" />
      <div className="Pokedex-List">
        <PokemonCardsWithModalError />
      </div>
      {(isLoading) ? <Spinner /> : null}
      {nextButton}
    </div>
  );
};

export default Pokedex;