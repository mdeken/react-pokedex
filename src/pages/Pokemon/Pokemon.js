import React, { useEffect, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as actions from '../../store/actions';

import './Pokemon.scss';

import { ReactComponent as BackArrow } from '../../assets/icons/arrow.svg';
import Spinner from '../../components/Spinner/Spinner';
import PokemonDetails from '../../components/PokemonDetails/PokemonDetails';
import Button from '../../components/Button/Button';
import withModalError from '../../hoc/withModalError';

const Pokemon = () => {
  const { id } = useParams();

  // REDUX state
  const pokemon = useSelector(state => state.selectedPokemon);
  const isLoading = useSelector(state => state.loading);
  const error = useSelector(state => state.error);

  // REACT state
  const [pokemonVariety, setPokemonVariety] = useState(0);

  // REDUX dispatch
  const dispatch = useDispatch();
  const fetchPokemonDetails = useCallback((id) => dispatch(actions.fetchPokemon(id)), [dispatch]);
  const clearPokemonDetails = useCallback(() => dispatch(actions.clearPokemon()), [dispatch]);

  useEffect(() => {
    fetchPokemonDetails(id);
    return clearPokemonDetails;
  }, [id, fetchPokemonDetails, clearPokemonDetails]);

  const handleVarietyChange = (varietyIndex) => {
    setPokemonVariety(varietyIndex);
  } 
  
  const modalContent = (
    <div className="Modal-Content">
      <h2>Ooops...</h2>
      <p>An error occured...Retry later or contact Professor Oak.</p>
    </div>
  );
  const PokemonDetailsWithModal = withModalError(
    () => pokemon ? <PokemonDetails pokemon={pokemon} variety={pokemonVariety} /> : null,
    error,
    modalContent
  );

  const classes = ['Pokemon'];
  let varietyButtons = null;

  if (pokemon) {
    classes.push(pokemon.varieties[pokemonVariety].type1);
    
    varietyButtons = pokemon.varieties.map((variety, i) =>
      (<Button
          key={variety.id}
          className={i === pokemonVariety ? 'active' : null}
          clicked={() => handleVarietyChange(i)}
        >
          <img src={variety.sprite} alt={variety.name + ' button'}/>
      </Button>)
    );
  }

  return (
    <div className={classes.join(' ')}>
      <Link className="Back-Arrow" to="/"><BackArrow /></Link>
      <div className="Varieties">
        {varietyButtons && varietyButtons.length > 1 ? varietyButtons : null}
      </div>
      { isLoading ? <Spinner /> : <PokemonDetailsWithModal /> }
    </div>
  );
};

export default Pokemon;