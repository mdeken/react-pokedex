import React from 'react';
import './App.scss';
import { Route, Switch, useLocation } from 'react-router-dom';

import Pokedex from './pages/Pokedex/Pokedex';
import Pokemon from './pages/Pokemon/Pokemon';

function App() {
  const location = useLocation();
  
  return (
    <div className="App">
        <Switch location={location}>
          <Route exact path="/" component={Pokedex} />
          <Route path="/:id" component={Pokemon} />
        </Switch>
    </div>
  );
}

export default App;
