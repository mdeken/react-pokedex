import * as actions from './actionType';

const initialState = {
  pokemons: [],
  loading: true,
  page: 0,
  error: null,
  selectedPokemon: null
};

const fetchPokedexStart = (state, action) => {
  return {
    ...state,
    loading: true,
    error: null,
  };
};

const fetchPokedexSuccess = (state, action) => {
  return {
    ...state,
    loading: false,
    pokemons: state.pokemons.concat(action.pokemons),
    page: action.page
  };
};

const fetchPokedexFail = (state, action) => {
  return {
    ...state,
    loading: false,
    error: action.error,
  };
};

const fetchPokemonSuccess = (state, action) => {
  return {
    ...state,
    loading: false,
    selectedPokemon: action.pokemon
  };
};

const fetchPokemonFail = (state, action) => {
  return {
    ...state,
    loading: false,
    selectedPokemon: null,
    error: action.error,
  }
}

const clearPokemon = (state, action) => {
  return {
    ...state,
    selectedPokemon: null
  };
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actions.FETCH_POKEDEX_START: return fetchPokedexStart(state, action);
    case actions.FETCH_POKEDEX_SUCCESS: return fetchPokedexSuccess(state, action);
    case actions.FETCH_POKEDEX_FAIL: return fetchPokedexFail(state, action);
    case actions.FETCH_POKEMON_SUCCESS: return fetchPokemonSuccess(state, action);
    case actions.FETCH_POKEMON_FAIL: return fetchPokemonFail(state, action);
    case actions.CLEAR_POKEMON: return clearPokemon(state, action);
    default: return state;
  }
};

export default reducer;