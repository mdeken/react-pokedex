import { takeLeading, put, all, call, takeLatest } from 'redux-saga/effects';
import * as actions from './actions';
import * as actionTypes from './actionType';
import * as api from './api';

function* fetchPokedex(action) {
  if (action.page !== null) {
    try {
      yield put(actions.fetchPokedexStart());
      const response = yield call(api.getAllPokemon, action.page);

      // Extracting the pokemon urls for more informations
      const pokemonRequests = response.results.map((pokemon) => call(api.getPokemonById, pokemon.name));
      const pokemonResponse = yield all(pokemonRequests);

      const nextPage = (response.next) ? action.page + 1 : null;
      yield put(actions.fetchPokedexSuccess(pokemonResponse, nextPage));
    } catch(error) {
      yield put(actions.fetchPokedexFail(error));
    }
  }
};

function* fetchPokemon(action) {
  try {
    yield put(actions.fetchPokemonStart());
    const response = yield call(api.getPokemonSpeciesById, action.id);
    yield put(actions.fetchPokemonSuccess(response));
  } catch(error) {
    yield put(actions.fetchPokemonFail(error));
  }
}

export function* watchPokedex() {
  yield takeLeading(actionTypes.FETCH_POKEDEX, fetchPokedex);
  yield takeLatest(actionTypes.FETCH_POKEMON, fetchPokemon);
}