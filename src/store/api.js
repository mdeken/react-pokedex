import Pokemon from '../model/Pokemon';
import PokemonSpecies from '../model/PokemonSpecies';

const DEFAULT_LIMIT = 100;
const BASE_URL = 'https://pokeapi.co/api/v2';

export const getAllPokemon = async (page) => {
  // The PokeApi will return more pokemon than the 'official' pokedex.
  // We don't want to fetch them.
  const limit = (DEFAULT_LIMIT * page + DEFAULT_LIMIT > 807) ? 807 - DEFAULT_LIMIT * page : DEFAULT_LIMIT;
  const url = `${BASE_URL}/pokemon/?limit=${limit}&offset=${DEFAULT_LIMIT * page}`;

  return fetch(url)
    .then(response => {
      return new Promise((resolve, reject) => {
        if (response.status === 200) {
          response.json().then(json => resolve(json));
        } else {
          const error = new Error(response.status);
          reject(error);
        }
      });
    })
};

// Pokemon can also be get by name
export const getPokemonById = async (id) => {
  const url = `${BASE_URL}/pokemon/${id}`;
  
  return fetch(url)
    .then(response => {
      return new Promise((resolve, reject) => {
        if (response.status === 200) {
          response.json().then(json => resolve(new Pokemon(json)));
        } else {
          const error = new Error(response.status);
          reject(error);
        }
      });
    });
};

export const getPokemonSpeciesById = async (id) => {
  const url = `${BASE_URL}/pokemon-species/${id}`;

  return fetch(url)
    .then(response => response.json())
    .then(data => {
      const pokemonUrls = data.varieties.map(pokemon => getPokemonById(pokemon.pokemon.name));

      return Promise.all(pokemonUrls)
        .then(pokemons => {
          // Add custom sprites for the alt varieties
          pokemons.forEach((pokemon, i) => {
            if (i !== 0) {
              pokemon.sprite = `http://assets.pokemon.com/assets/cms2/img/pokedex/full/${pokemons[0].paddedId}_f${i + 1}.png`;
            }
          });
          return pokemons;
        })
        .then(pokemons => {
          data.varieties = pokemons;
          return new PokemonSpecies(data);
        });
    });
}