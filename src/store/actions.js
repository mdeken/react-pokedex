import * as actions from './actionType';

export const fetchPokedexStart = () => {
  return {
    type: actions.FETCH_POKEDEX_START
  };
};

export const fetchPokedex = (page) => {
  return {
    type: actions.FETCH_POKEDEX,
    page: page
  };
};

export const fetchPokedexSuccess = (pokemons, page) => {
  return {
    type: actions.FETCH_POKEDEX_SUCCESS,
    pokemons: pokemons,
    page: page
  };
};

export const fetchPokedexFail = (error) => {
  return {
    type: actions.FETCH_POKEDEX_FAIL,
    error: error
  };
};

export const fetchPokemonStart = () => {
  return {
    type: actions.FETCH_POKEMON_START,
  };
};

export const fetchPokemon = (pokemonId) => {
  return {
    type: actions.FETCH_POKEMON,
    id: pokemonId,
  };
};

export const fetchPokemonSuccess = (pokemon) => {
  return {
    type: actions.FETCH_POKEMON_SUCCESS,
    pokemon: pokemon,
  };
};

export const fetchPokemonFail = (error) => {
  return {
    type: actions.FETCH_POKEMON_FAIL,
    error: error,
  };
};

export const clearPokemon = () => {
  return {
    type: actions.CLEAR_POKEMON,
  };
};