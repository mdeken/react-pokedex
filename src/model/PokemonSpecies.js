const PokemonSpecies = function(data) {
  this.id = data.id;
  this.paddedId = String(this.id).padStart(3, '0');
  this.name = data.name;
  this.japaneseName = data.names.find(entry => entry.language.name === 'ja').name;
  this.extendedDescription = data.flavor_text_entries.find(entry => entry.language.name === 'en').flavor_text;
  this.shortDescription = data.genera.find(entry => entry.language.name === 'en').genus;
  this.varieties = data.varieties;
};

export default PokemonSpecies;