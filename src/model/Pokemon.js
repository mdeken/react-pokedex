const Pokemon = function(data) {
  this.id = data.id;
  this.paddedId = String(this.id).padStart(3, '0');
  this.name = data.name;
  this.type1 = (data.types[1]) ? data.types[1].type.name : data.types[0].type.name;
  this.type2 = (data.types[1]) ? data.types[0].type.name : null;
  // Didn't like the pixelated sprites of PokeApi... So we're gonna use the ones available on pokemon website
  // The website don't have sprites for pokemon above id 807, we will remove any pokemon id above 807
  this.sprite = (data.id <= 807) ?
    `http://assets.pokemon.com/assets/cms2/img/pokedex/full/${this.paddedId}.png` :
    null;
  this.weight = data.weight;
  this.height = data.height;
  this.stats = data.stats.map(stat => ({base_stat: stat.base_stat, name: stat.stat.name}));
};

export default Pokemon;