import React, { useRef, useEffect } from 'react';
import {TweenMax, Elastic} from 'gsap/TweenMax';

import {ReactComponent as Pokeball} from '../../assets/icons/pokeball.svg';

const Spinner = () => {
  const spinnerRef = useRef(null);

  useEffect(() => {
    TweenMax.to(spinnerRef.current, 3, {
      rotation: 360,
      repeat: -1,
      ease: Elastic.easeOut.config(1, 0.4),
    })
  }, []);

  return (
    <div className="Spinner" ref={spinnerRef}>
      <Pokeball />
    </div>
  );
};

export default Spinner;