import React from 'react';
import PropTypes from 'prop-types';

import './Chips.scss';

const Chips = (props) => {
  return (
    <div className="Chips">
      <p>{props.text}</p>
    </div>
  );
};

Chips.propTypes = {
  text: PropTypes.string.isRequired
};

export default Chips;