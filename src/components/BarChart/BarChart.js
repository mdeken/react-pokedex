import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

import './BarChart.scss';

const draw = (data, ref, xAccess, yAccess) => {
  const barChart = d3.select(ref)
  
  const bars = barChart.selectAll('.barContainer').data(data)
    .enter()
      .append('div')
      .attr('class', 'barContainer');

    bars.append('p')
      .attr('class', 'label')
      .text(yAccess);

    bars.append('div')
      .attr('class', 'bar')
      .append('div')
      .attr('class', 'scale')
      .style('width', xAccess);

};

const BarChart = (props) => {
  const chartRef = useRef(null);
  
  useEffect(() => {
    draw(props.data, chartRef.current, props.xAccess, props.yAccess);
  }, [props.data, props.xAccess, props.yAccess]);
  
  return (
    <div className="BarChart" ref={chartRef}>
    </div>
  );
};

export default BarChart;