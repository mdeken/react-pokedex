import React from 'react';
import PropTypes from 'prop-types';
import {types} from '../../types';
import { Link } from 'react-router-dom';

import Chips from '../Chips/Chips';
import { ReactComponent as Pokeball } from '../../assets/icons/pokeball.svg';

import './Card.scss';

const Card = (props) => {
  const cardStyle = ["Card"];
  cardStyle.push(props.type1);

  return (
    <div className={cardStyle.join(' ')}>
      <Link to={'/' + props.id}>
        <div className="Card-container">
          <div className="Card-data">
            <div className="Card-name">
              <h2>{props.name}</h2>
              <h3>#{props.paddedId}</h3>
            </div>
            <div className="Card-type">
              <Chips text={props.type1} />
              { props.type2 ? <Chips text={props.type2} /> : null }
            </div>
          </div>
          <div className="Card-image">
            <img src={props.sprite} alt={props.name} />
          </div>
        <Pokeball className="Card-Pokeball" />
        </div>
      </Link>
    </div>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  type1: PropTypes.oneOf(types).isRequired,
  type2: PropTypes.oneOf(types),
  sprite: PropTypes.string.isRequired
}

export default React.memo(Card);