import React from 'react';

import './PokemonDetails.scss';

import BarChart from '../BarChart/BarChart';

const PokemonDetails = (props) => {
  const pokemon = {
    ...props.pokemon,
    ...props.pokemon.varieties[props.variety],
    paddedId: props.pokemon.paddedId
  };
  return (
    <div className="PokemonDetails">
      <div className="Header">
        <h1 className="id">#{pokemon.paddedId}</h1>
        <h2>{pokemon.name}</h2>
      </div>
      <main className="Content">
        <div className="Details-wrapper">
          <div className="Details">
            <h3 className="Japanese-name">{pokemon.japaneseName}</h3>
            <p className="Short-description">{pokemon.shortDescription}</p>
              <p><span className="bold">Height</span> - {pokemon.height}</p>
              <p><span className="bold">Weight</span> - {pokemon.weight}</p>
              <p>{pokemon.extendedDescription}</p>
          </div>

          <div className="Image">
            <img src={pokemon.sprite} alt={pokemon.name}/>
          </div>
        </div>
        <div className="Stats">
          <h2>Stats</h2>
          <BarChart
            data={pokemon.stats}
            xAccess={d => (d.base_stat / 255) * 100 + '%'}
            yAccess={d => (`${d.name} - ${d.base_stat}`)}
          />
        </div>
      </main>
    </div>
  );
};

export default PokemonDetails;