import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = (props) => {
  return (
    <button className={"Button " + props.className} onClick={props.clicked} >{props.children}</button>
  );
};

Button.propTypes = {
  clicked: PropTypes.func
};

export default Button;