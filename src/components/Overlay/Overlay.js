import React from 'react';

import './Overlay.scss';

const Overlay = (props) => {
  return (
    <div
      className="Overlay"
      onClick={props.clicked}
    ></div>
  );
};

export default Overlay;