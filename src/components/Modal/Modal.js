import React from 'react';

import './Modal.scss';

import Overlay from '../Overlay/Overlay';

const Modal = (props) => {
  return (
    <React.Fragment>
      <Overlay clicked={props.close}/>
      <div className="Modal" onClick={props.close}>
        {props.children}
      </div>
    </React.Fragment>
  );
};

export default Modal;